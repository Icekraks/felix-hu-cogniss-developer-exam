const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const inputFile1 = './input2.json';
const outputFile = './output2.json';

jsonfile.readFile(inputFile1, function(err, body){
	console.log(body);
	let output={};
	output.emails = [];
	body.names.forEach(name=>{
		output.emails.push(name.split('').reverse().join('')+randomstring.generate(5)+'@gmail.com');
	})
	jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
		console.log("All done!");
	});
})
